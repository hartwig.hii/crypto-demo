import React, {Component} from 'react';
import {Button, ButtonToolbar} from 'react-bootstrap';
import {AddModal} from './AddModal';

export class Crypto extends Component {

      constructor(props) {
        super(props);
        this.state = {coins:[], addModalShow:false}
      }

      componentDidMount() {
        this.refresList();
      }

      refresList() {
        fetch('http://localhost:8080/crypto')
        .then(res => res.json())
        .then((data) => {
            this.setState({ coins: data });
        })
        .catch(console.log);
      }
        
      componentDidUpdate() {
        this.refresList();
      }

      deleteCoin(coinId) {
        if(window.confirm('Are you sure?')) {
          fetch('http://localhost:8080/crypto/' + coinId, {
            method: 'DELETE',
            header: {
              'Accept':'application/json',
              'Content-Type':'application/json'
            }
          })
        }
      }

  render() {

    const {coins} = this.state;
    let addModalClose = () => this.setState({addModalShow:false});

    return(
      <div className="container">
      <table className="table">
      <thead>
        <tr>
          <th>Cryptocurrency</th>
          <th>Amount</th>
          <th>Date of purchase</th>
          <th>Wallet location</th>
          <th>Current market value (EUR)</th>
          <th>Option</th>
        </tr>
      </thead>
      <tbody>
      {coins.map((coins) => (
        <tr key={coins.id}>
          <td>{coins.coinName}</td>
          <td>{coins.amount}</td>
          <td>{coins.dateOfPurchase}</td>
          <td>{coins.coinLocation}</td>
          <td>{coins.currentMarketValue}</td>
          <td>
          <ButtonToolbar>
          
          <Button className="mr-2" onClick={() => this.deleteCoin(coins.id)} variant="danger">Delete</Button>

          </ButtonToolbar>

          </td>
        </tr>
      ))}
      </tbody>
    </table>
    <ButtonToolbar>
      <Button
      variant='primary' 
      onClick={()=> this.setState({addModalShow: true})}
      > Add currency
      </Button>
      
      <AddModal
      show={this.state.addModalShow}
      onHide={addModalClose}
      />      
  
    </ButtonToolbar>
    </div>
    )
  }

}
