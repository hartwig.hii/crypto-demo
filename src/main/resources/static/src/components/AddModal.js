import React, { Component } from 'react';
import {Modal, Button, Row, Col, Form} from 'react-bootstrap';



export class AddModal extends Component {
    constructor(props) {
        super(props);
    }

    handleSubmit(event) {
        event.preventDefault();
        
        fetch('http://localhost:8080/crypto',{
            method: 'POST',
            headers: {
                'Accept':'application/json',
                'Content-Type':'application/json'
            },
            body:JSON.stringify({
                coinName:event.target.coinname.value,
                dateOfPurchase:event.target.date.value,
                coinLocation:event.target.loc.value,
                amount:event.target.amount.value
            })
        })
    }

    render() {
        return(
            <Modal
            {...this.props}
            size="lg"
            aria-labelledby="contained-modal-title-vcenter"
            centered
          >
            <Modal.Header closeButton>
              <Modal.Title id="contained-modal-title-vcenter">
                Add cryptocurrency
              </Modal.Title>
            </Modal.Header>
            <Modal.Body>
              <div className="container">
                <Row>
                    <Col>
                        <Form onSubmit={this.handleSubmit}>
                            <Form.Group controlId="coinname">
                            <Form.Control as="select">
                            <option>Bitcoin</option>
                            <option>Ethereum</option>
                            <option>Ripple</option>
                            </Form.Control>
                            </Form.Group>
                            <Form.Group controlId="date">
                                <Form.Control
                                    type="text"
                                    name="date"
                                    required
                                    placeholder="20.06.2018"
                                />
                            </Form.Group>
                            <Form.Group controlId="loc">
                                <Form.Control
                                    type="text"
                                    name="loc"
                                    required
                                    placeholder="stored on D:/"
                                />
                            </Form.Group>
                            <Form.Group controlId="amount">
                                <Form.Control
                                    type="text"
                                    name="amount"
                                    required
                                    placeholder="10"
                                />
                            </Form.Group>
                            <Form.Group>
                                <Button variant='primary' type='submit'>Add cryptocurrency</Button>
                            </Form.Group>
                        </Form>
                    </Col>
                </Row>
              </div>
            </Modal.Body>
            <Modal.Footer>
              <Button variant="danger" onClick={this.props.onHide}>Close</Button>
            </Modal.Footer>
          </Modal>
        )
    }
}