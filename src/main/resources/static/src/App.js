// src/App.js

import React from 'react';
import {Crypto} from './components/Crypto';


function App() {
    return (
        <div className="container">
            <Crypto/>
        </div>
    )
}

// class App extends Component {

//     constructor(props) {
//         super(props);
//         this.state ={deps:[], addModalShow : false}
//     }

//     state = {
//         crypto: []
//     }

//     componentDidMount() {
//         fetch('http://localhost:8080/crypto')
//             .then(res => res.json())
//             .then((data) => {
//                 this.setState({ crypto: data })
//             })
//             .catch(console.log)
//     }

//     render() {
//         return (
//             <Crypto crypto={this.state.crypto} />
            
//         )
//     }

// }

export default App;