package CryptoPortfolio.Demo.model;

public class Portfolio {
    private int id;
    private String coinName;
    private String dateOfPurchase;
    private String coinLocation;
    private double amount;
    private double currentMarketValue;

    public Portfolio(int id, String coinName, String dateOfPurchase, String coinLocation, double amount) {
        this.id = id;
        this.coinName = coinName;
        this.dateOfPurchase = dateOfPurchase;
        this.coinLocation = coinLocation;
        this.amount = amount;
    }

    public double getCurrentMarketValue() {
        return currentMarketValue;
    }

    public void setCurrentMarketValue(double currentMarketValue) {
        this.currentMarketValue = currentMarketValue;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCoinName() {
        return coinName;
    }

    public void setCoinName(String coinName) {
        this.coinName = coinName;
    }

    public String getDateOfPurchase() {
        return dateOfPurchase;
    }

    public void setDateOfPurchase(String dateOfPurchase) {
        this.dateOfPurchase = dateOfPurchase;
    }

    public String getCoinLocation() {
        return coinLocation;
    }

    public void setCoinLocation(String coinLocation) {
        this.coinLocation = coinLocation;
    }
}
