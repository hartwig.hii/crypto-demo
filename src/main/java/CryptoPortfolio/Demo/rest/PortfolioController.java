package CryptoPortfolio.Demo.rest;

import CryptoPortfolio.Demo.model.Portfolio;
import CryptoPortfolio.Demo.repository.PortfolioRepo;
import CryptoPortfolio.Demo.service.BitfinexService;
import CryptoPortfolio.Demo.service.PortfolioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/crypto")
@CrossOrigin("*")
public class PortfolioController {

    @Autowired
    private PortfolioRepo portfolioRepo;

    @Autowired
    private PortfolioService portfolioService;

    @GetMapping
    public List<Portfolio> getAllPortfolios() {
        return portfolioService.getPorfolios();
    }

    @PostMapping
    public void addPortfolioCoin(@RequestBody Portfolio portfolio) {
        portfolioRepo.addPortfolioCoin(portfolio);
    }

    @DeleteMapping("/{id}")
    public void deletePortfolioCoin(@PathVariable("id") int id) {
        portfolioRepo.deletePortfolioCoin(id);
    }

}
