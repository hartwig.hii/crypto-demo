package CryptoPortfolio.Demo.service;

import CryptoPortfolio.Demo.model.Portfolio;
import CryptoPortfolio.Demo.repository.PortfolioRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.sound.sampled.Port;
import java.util.List;

@Service
public class PortfolioService {

    @Autowired
    private PortfolioRepo portfolioRepo;

    @Autowired
    private BitfinexService bitfinexService;

    public List<Portfolio> getPorfolios() {
        List<Portfolio> portfolios = portfolioRepo.getPorfolios();

        for (Portfolio portfolioItem : portfolios) {
            portfolioItem.setCurrentMarketValue(
                    bitfinexService.getCurrentMarketValue(
                            portfolioItem.getAmount(),
                            portfolioItem.getCoinName()));
        }
        return portfolios;
    }
}
