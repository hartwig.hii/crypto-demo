package CryptoPortfolio.Demo.service;

import org.springframework.stereotype.Service;

import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;

@Service
public class BitfinexService {
    //java.net.httpClient
    //Should try/catch this..
    HttpClient client = HttpClient.newHttpClient();
    HttpRequest request = HttpRequest.newBuilder().uri(URI.create("https://api-pub.bitfinex.com/v2/tickers?symbols=tBTCEUR,tETHEUR,tXRPUSD")).build();
    String myInfo = client.sendAsync(request, HttpResponse.BodyHandlers.ofString())
            .thenApply(HttpResponse::body)
            .join();


    String[] coins = myInfo.split(",");

    // find a better way to do this
    double btcUsd = Double.parseDouble(coins[7]);
    double ethUsd = Double.parseDouble(coins[18]);
    double xrpUsd = Double.parseDouble(coins[29]);

    public double sentBtc() {
        return btcUsd;
    }
    public double sentEth() {
        return ethUsd;
    }
    public double sentXrp() {
        return xrpUsd;
    }

    public double getCurrentMarketValue(double amount, String coinName) {
        switch (coinName) {
            case "Bitcoin":
                return amount * sentBtc();
            case "Ethereum":
                return amount * sentEth();
            case "Ripple":
                return (amount * sentXrp()) * 0.907; //to Eur.. NB! should not be static.

            default:
                return 0;
        }
    }
}
