package CryptoPortfolio.Demo.repository;

import CryptoPortfolio.Demo.model.Portfolio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class PortfolioRepo {

    @Autowired
    private JdbcTemplate jdbcTemplate;



    public List<Portfolio> getPorfolios() {
        return jdbcTemplate.query("select * from coin", (row, number) -> {
            return new Portfolio(
                    row.getInt("id"),
                    row.getString("coinname"),
                    row.getString("dateofpurchase"),
                    row.getString("coinlocation"),
                    row.getDouble("amount")
                    //this.getCurrentMarketValue(row.getDouble("amount"), row.getString("coinname"))
            );
        });
    }

    public void addPortfolioCoin(Portfolio portfolio) {
        jdbcTemplate.update("insert into coin (coinName, dateOfPurchase, coinLocation, amount) values(?, ?, ?, ?)",
                portfolio.getCoinName(), portfolio.getDateOfPurchase(), portfolio.getCoinLocation(), portfolio.getAmount()
        );
    }

    public void deletePortfolioCoin(int id) {
        jdbcTemplate.update("delete from coin where id = ?", id);
    }
}
